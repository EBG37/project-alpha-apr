from django.forms import ModelForm, DateInput
from tasks.models import Task

class DateInput(DateInput):
    input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "notes",
            "project",
            "assignee",
        ]


class EditTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "notes",
            "project",
            "assignee",
            "is_completed",
        ]
        widgets = {
            "start_date": DateInput,
            "due_date": DateInput
        }