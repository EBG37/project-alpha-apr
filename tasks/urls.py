from django.urls import path
from tasks.views import create_task, my_task_list, edit_task


urlpatterns = [
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("mine/", my_task_list, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
