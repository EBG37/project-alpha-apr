from django.shortcuts import render, redirect
from tasks.forms import TaskForm, EditTaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def my_task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/task_list.html", context)


@login_required
def edit_task(request, id):
    post = Task.objects.get(id=id)
    if request.method == "POST":
        form = EditTaskForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_project", id=post.project.id)
    else:
        form = EditTaskForm(instance=post)

        context = {"form": form}
        return render(request, "tasks/edit_task.html", context)
