from django.urls import path
from projects.views import (
    show_project,
    show_project_detail,
    create_project,
    edit_project,
    delete_project,
)


urlpatterns = [
    path("<int:id>/delete/", delete_project, name="delete_project"),
    path("<int:id>/edit/", edit_project, name="edit_project"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project_detail, name="show_project"),
    path("", show_project, name="list_projects"),
]
